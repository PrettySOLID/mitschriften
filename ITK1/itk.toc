\contentsline {section}{\numberline {1}Codierung und Decodierung}{2}{}%
\contentsline {subsection}{\numberline {1.1}Quellencodierung}{2}{}%
\contentsline {subsubsection}{\numberline {1.1.1}Analoge Signale}{2}{}%
\contentsline {subsubsection}{\numberline {1.1.2}Digitale Signale}{2}{}%
\contentsline {subsection}{\numberline {1.2}Kanalcodierung}{4}{}%
\contentsline {subsection}{\numberline {1.3}Modulation}{4}{}%
\contentsline {subsection}{\numberline {1.4}übertragungskanal}{4}{}%
\contentsline {subsection}{\numberline {1.5}Demodulation}{4}{}%
\contentsline {subsection}{\numberline {1.6}Kanaldecodierung}{4}{}%
\contentsline {subsection}{\numberline {1.7}Quellendecodierung}{4}{}%
\contentsline {section}{\numberline {2}Logarithmische Skala}{5}{}%
\contentsline {section}{\numberline {3}Formeln und Beispielrechnungen}{5}{}%
\contentsline {subsection}{\numberline {3.1}Formeln}{5}{}%
\contentsline {subsection}{\numberline {3.2}Beispiele}{6}{}%
\contentsline {subsubsection}{\numberline {3.2.1}Beispiel 1}{6}{}%
\contentsline {subsubsection}{\numberline {3.2.2}Beispiel 2}{7}{}%
\contentsline {section}{\numberline {4}Ablesen und einzeichen von Werten in \\Darstellungen}{8}{}%
\contentsline {subsection}{\numberline {4.1}Lineare Darstellung}{8}{}%
\contentsline {subsection}{\numberline {4.2}Logarithmische Darstellung}{9}{}%
